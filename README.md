# OTP Visualizer

Just visualize your OTP application to know more instinctively.

# Memo

- `gen_fsm, gen_server, supervisor, gen_event and application`
- [Original gen_fsm visualizer](https://gist.github.com/kuenishi/42e1a013cec4adfecc82)
