-module('visotp').

%% API exports
-export([main/1]).

%%====================================================================
%% API functions
%%====================================================================

-define(CONSOLE(Fmt, Terms), io:format(Fmt, Terms)).
-define(CONSOLE(String), io:format(String, Terms)).

%% escript Entry point
main(Args) ->
    _ = application:load(lager),
    {ok, _App} = application:ensure_all_started(lager),
    ok = application:set_env(lager, handlers, [{lager_console_backend, info}]),

    case getopt:parse(option_spec(), Args) of
        {ok, {Options, []}} ->
            case proplists:get_value(file, Options) of
                undefined ->
                    getopt:usage(option_spec(), "visotp", "<applicatoin path>");
                File ->
                    gen_fsm_visualizer:main([File])
            end;

        {ok, {Options, [Path]}} ->
            ?CONSOLE("Options: ~p\tPath: ~p~n", [Options, Path]),
            {ok, AppDirFiles}  = file:list_dir(Path),
            true = lists:member("src", AppDirFiles),
            SrcDir = filename:join(Path, "src"),
            {ok, Sources0} = file:list_dir(SrcDir),
            Sources = lists:filter(fun(Filename) ->
                                           ".erl" =:= filename:extension(Filename)
                                   end, Sources0),
            [AppFile] = lists:filter(fun(Filename) ->
                                             ".src" =:= filename:extension(Filename)
                                     end, Sources0),
            ?CONSOLE("SrcDir: ~p~n", [Sources]),
            ?CONSOLE("AppFile: ~p~n", [AppFile]),
            [gen_fsm_visualizer:main([filename:join([SrcDir, SrcFile])])
             || SrcFile <- Sources];

        {ok, {_Options, _}} ->
            getopt:usage(option_spec(), "visotp", "<applicatoin path>");
        {error, _} ->
            getopt:usage(option_spec(), "visotp")
    end.

%%====================================================================
%% Internal functions
%%====================================================================

option_spec() ->
    [{file, $f, "file", file, "Analyze single file"},
     {eval, $e, "eval", string, "Evaluation mode"}].
